
[Download da mini-iso](https://www.debian.org/CD/netinst/)

[Imagem do CD netinst amd64](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso)

![img/debian-server-create-new-vm.png](./img/debian-server-create-new-vm.png)

![img/debian-server-memory-size.png](./img/debian-server-memory-size.png)

![img/debian-server-01-install-virtual-machine.md](./img/debian-server-01-install-virtual-machine.md)

![img/debian-server-hard-disk-file-type.png](./img/debian-server-hard-disk-file-type.png)

![img/debian-server-hard-disk-storage.png](./img/debian-server-hard-disk-storage.png)

![img/debian-server-hard-disk-file-size.png](./img/debian-server-hard-disk-file-size.png)

![img/debian-server-network-adapter.png](./img/debian-server-network-adapter.png)

![img/debian-server-choose-iso-file.png](./img/debian-server-choose-iso-file.png)

![img/debian-server-choose-iso-file-2.png](./img/debian-server-choose-iso-file-2.png)

![img/debian-server-selected-iso-file.png](./img/debian-server-selected-iso-file.png)

Clicar em Start para iniciar a instalação.

![img/debian-server-graphical-install.png](./img/debian-server-graphical-install.png)

![img/debian-server-select-a-language.png](./img/debian-server-select-a-language.png)

![img/debian-server-locality.png](./img/debian-server-locality.png)

![img/debian-server-keyboard.png](./img/debian-server-keyboard.png)

![img/debian-server-hostname.png](./img/debian-server-hostname.png)

![img/debian-server-domain-name.png](./img/debian-server-domain-name.png)

![img/debian-server-root-password.png](./img/debian-server-root-password.png)

![img/debian-server-user-name.png](./img/debian-server-user-name.png)

![img/debian-server-account-user-name.png](./img/debian-server-account-user-name.png)

![img/debian-server-user-password.png](./img/debian-server-user-password.png)

![img/debian-server-clock-setting.png](./img/debian-server-clock-setting.png)

![img/debian-server-disk-partitions.png](./img/debian-server-disk-partitions.png)

![img/debian-server-disk-partitions-2.png](./img/debian-server-disk-partitions-2.png)

![img/debian-server-disk-partitions-3.png](./img/debian-server-disk-partitions-3.png)

![img/debian-server-disk-partitions-4.png](./img/debian-server-disk-partitions-4.png)

![img/debian-server-disk-partitions-5.png](./img/debian-server-disk-partitions-5.png)

![img/debian-server-installing.png](./img/debian-server-installing.png)

![img/debian-server-local-packages.png](./img/debian-server-local-packages.png)

![img/debian-server-remote-packages.png](./img/debian-server-remote-packages.png)

![img/debian-server-remote-packages-mirror.png](./img/debian-server-remote-packages-mirror.png)

![img/debian-server-proxy.png](./img/debian-server-proxy.png)

![img/debian-server-install-packages.png](./img/debian-server-install-packages.png)

![img/debian-server-popularity-contest.png](./img/debian-server-popularity-contest.png)

![img/debian-server-packages-selection.png](./img/debian-server-packages-selection.png)

![img/debian-server-grub.png](./img/debian-server-grub.png)

![img/debian-server-grub-device.png](./img/debian-server-grub-device.png)

![img/debian-server-install-finish.png](./img/debian-server-install-finish.png)

![img/debian-server-grub-boot.png](./img/debian-server-grub-boot.png)

![img/debian-server-first-boot.png](./img/debian-server-first-boot.png)





